<!-- Header -->
<?php include_once 'Views/Elements/header.php'; ?>
<!-- Sidebar -->
<?php include 'Views/Elements/default-sidebar.php'; ?>
<!-- Content -->
<main class="s-layout__content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="title_page"> Bonjour !</h1>
			</div>
			<div class="col-sm-12">
				<p> Cette application  a pour but de mettre en pratique  le modèle MVC en PHP par l'équipe de SIL3 de l'IUT2 de Grenoble. </p>
			</div>
			<div class="col-sm-12">
				<p> Elle vous permet de manipuler des photos, naviguer, partager, classer en album </p>
			</div>
		</div>
	<div>
</main>
<!-- Footer -->
<?php include 'Views/Elements/footer.php'; ?>