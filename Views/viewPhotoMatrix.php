<!-- Header -->
<?php include_once 'Views/Elements/header.php'; ?>
<!-- Sidebar -->
<?php include 'Views/Elements/sidebar.php'; ?>
<!-- Content -->
<main class="s-layout__content">
	<div class="container">
		<div class="row">
			<?php if(!isset($i[2])) { ?>
			<div class="col-sm-12">
                <?php # mise en place de la vue partielle : le contenu central de la page
                    # Mise en place des deux boutons
                    print "<a class=\"float-left\" href=\"index.php?controller=$controller&imgId=$imgId&size=$size&action=backward&nbimg=$nbImg&cat=".urlencode($catselect)."\"><i class=\"fa fa-arrow-left fa-3x display-arrow\"></i><span>Previous</span></a>";
                    print "<a class=\"float-right\" href=\"index.php?controller=$controller&imgId=$imgId&size=$size&action=forward&nbimg=$nbImg&cat=".urlencode($catselect)."\"><span class=\"next-span\">Next</span><i class=\"fa fa-arrow-right fa-3x display-arrow\"></i></a>";
                ?>
			</div>
			<?php } ?>
			<div class="col-sm-12 div-image">
    			<?php 
					# Affiche de la matrice d'image avec une reaction au click
					# Réalise l'affichage de l'image
					# Adapte la taille des images au nombre d'images présentes
					$size = 480 / sqrt(count($imgMatrixURL));
					# Affiche les images
					foreach ($imgMatrixURL as $i) {
						print "<a href=\"".$i[1]."\"><img src=\"".$i[0]."\" width=\"".$size."\" height=\"".$size."\"></a>\n";
						if (isset($i[2])){
							print "<a href=\"".$i[1]."&action=enlever\"><img src=out\less.png width = 40>Supprimer de l'album</a>\n";}
					};
				?>		
			</div>
		</div>
	<div>
</main>
<!-- Footer -->
<?php include 'Views/Elements/footer.php'; ?>