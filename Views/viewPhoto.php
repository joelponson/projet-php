<!-- Header -->
<?php include_once 'Views/Elements/header.php'; ?>
<!-- Sidebar -->
<?php include 'Views/Elements/sidebar.php'; ?>
<!-- Content -->
<main class="s-layout__content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
                <?php # mise en place de la vue partielle : le contenu central de la page
                    # Mise en place des deux boutons
                    print "<a class=\"float-left\" href=\"index.php?controller=photo&imgId=$imgId&size=$size&action=backward&cat=".urlencode($catselect)."\"><i class=\"fa fa-arrow-left fa-3x display-arrow\"></i><span>Previous</span></a>";
                    print "<a class=\"float-right\" href=\"index.php?controller=photo&imgId=$imgId&size=$size&action=forward&cat=".urlencode($catselect)."\"><span class=\"next-span\">Next</span><i class=\"fa fa-arrow-right fa-3x display-arrow\"></i></a>";
                ?>
            </div>
            <div class="col-sm-12 div-image">
                <?php
                    print"<h3>Catégorie : ". $cat."</h3>";
                    print "<h4>Commentaire : ".$Comment."</h4>";
                    # Affiche l'image avec une reaction au click
                    print "<a href=\"index.php?controller=photoModif&zoom=1.25&imgId=$imgId&size=$size\">";
                    // Réalise l'affichage de l'image
                    print "<img src=\"$imgURL\" width=\"$size\"></a>\n";
                ?>
            </div>
            <div class="col-sm-12 div-liked">
                <?php
                    print "<a href=\"index.php?controller=photo&zoom=1.25&imgId=$imgId&size=$size&action=like\">$like";
                    print "<i class=\"fa fa-thumbs-up fa-2x display-liked\"></i><span>J'aime</span></a>";
                    print "<a class=\"display-disliked\" href=\"index.php?controller=photo&zoom=1.25&imgId=$imgId&size=$size&action=dislike\">$dislike";
                    print "<i class=\"fa fa-thumbs-down fa-2x fa-2x display-liked\"></i><span>J'aime pas</span></a>";
                ?>
            </div>
            <div class="col-sm-12 div-album">
                <?php
                    print "<form name=album method=get action=index.php?>";
                    print "<input type=hidden name=imgId value=$imgId />";
                    print "<input type=hidden name=size value=$size />";
                    print "<input type=hidden name=nbimg value=$nbImg />";
                    print "<input type=hidden name=controller value=photo />";    
                    print "<input type=hidden name=action value=ajout />";     
                    print "<select name=album id=album>";
                    foreach ($albumListe as $nomAlbum => $nomAlbum2) {
                        if (!empty($nomAlbum) && isset($nomAlbum)) {
                            print "<option>$nomAlbum</option>";
                        }
                    }
                    print "</select>";
                ?>
                <input type="submit" value="Ajouter à l'album">
                </form>
			</div>
        </div>
	<div>
</main>
<!-- Footer -->
<?php include 'Views/Elements/footer.php'; ?>




