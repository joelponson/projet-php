<!-- Header -->
<?php include_once 'Views/Elements/header.php'; ?>
<!-- Sidebar -->
<?php include 'Views/Elements/sidebar.php'; ?>
<!-- Content -->
<main class="s-layout__content">
	<div class="container">
		<div class="row">
            <div class="col-sm-12 div-image">
                <?php 
                    if (!$ok){
                        print"<i class=\"fa fa-upload fa-3x upload-icon\"></i>";
                    }else { 
                        print"<i class=\"fa fa-check fa-3x upload-icon\"></i>" ; 
                    }
                ?>
                <form method="post" action=index.php? enctype="multipart/form-data">
                    <fieldset>
                        <legend>Information Photo :</legend>
                        <label for="cat">Votre categorie :</label>
                        <?php 
                            print " <input id=cat type=Text list=listecat name=cat value= $cat> ";
                            print " <datalist id=listecat name=listecat >";

                            foreach ($catliste as $cats=>$item) {
                                if ($item == $cat){
                                    print "<option selected value= $item>";
                                } else {
                                    print "       <option value= $item>";
                                } 
                            } 
                        ?>
                        </datalist> 
                        <br/>
                        <label for="comment">Votre commentaire :</label>
                        <textarea name="comment" id = "comment"></textarea>   
                        <input type=hidden name=action value=upload />    
                        <input type=hidden name=controller value=photoModif />  
                        <!-- On limite le fichier à 100Ko -->
                        <input type="hidden" name="MAX_FILE_SIZE" value="100000"><br/><br/>
                        Fichier : <input type="file" name="avatar">
                        <input type="submit" name="envoyer" value="Envoyer le fichier">
                    </fieldset>
                </form>
            
            </div>
		</div>
	<div>
</main>
<!-- Footer -->
<?php include 'Views/Elements/footer.php'; ?>