<?php # Mise en place du menu par un parcours de la table associative
    $menu['Home']="index.php";
    $menu['A propos']="index.php";
    $menu['Une à une']="index.php?controller=photo&action=first&imgId=$imgId&size=$size&cat=".urlencode($catselect);	
    $menu['First']="index.php?controller=$controller&action=first&imgId=$imgId&nbimg=$nbImg&size=$size&cat=".urlencode($catselect);		
    $menu['Random']="index.php?controller=$controller&action=random&zoom=1.25&imgId=$imgId&nbimg=$nbImg&size=$size&cat=".urlencode($catselect);
    # Pour afficher moins d'image passe à une autre page
    if (($nbImg/2)>=1 ){
        $menu['Less']="index.php?controller=photoMatrix&imgId=$imgId&action=less&nbimg=$nbImg&cat=".urlencode($catselect); 
    } else{
        $menu['Less']="index.php?controller=photo&imgId=$imgId&action=less&nbimg=$nbImg&cat=".urlencode($catselect);                
    }     
    # Pour afficher plus d'image passe à une autre page
    $menu['More']="index.php?controller=photoMatrix&imgId=$imgId&action=more&nbimg=$nbImg&cat=".urlencode($catselect);
    // Demande à calculer un zoom sur l'image
    $menu['Zoom +']="index.php?controller=photo&action=zoomm&zoom=1.25&imgId=$imgId&size=$size&cat=".urlencode($catselect);
    // Demande à calculer un zoom sur l'image
    $menu['Zoom -']="index.php?controller=photo&action=zooml&zoom=1.25&imgId=$imgId&size=$size&cat=".urlencode($catselect);
    // Demande à ajouter une image
    $menu['Ajouter une image']="index.php?controller=photoModif&action=upload";
    # Pour album
    $menu['Album image']="index.php?controller=photoAlbum&imgId=$imgId";
?>
<!-- Sidebar -->
<div class="s-layout__sidebar">
  	<a class="s-sidebar__trigger" href="#0">
     	<i class="fa fa-bars"></i>
  	</a>
    <nav class="s-sidebar__nav">
		<ul>
            <?php
            	foreach ($menu as $item => $act) {
                    switch ($item) {
                        case 'Home':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-home\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'A propos':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-info\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Une à une':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-arrows-alt-h\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'First':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-image\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Random':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-random\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Less':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-image\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'More':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-images\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Zoom +':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-search-plus\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Zoom -':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-search-minus\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Ajouter une image':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-camera\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Album image':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-folder-open\"></i><em>$item</em></a></li>\n";
                            break;
                    }
                }
            ?>
            <?php
                print "Catégorie";
                print "<form name=form1 method=get action=index.php?>";
                print "<input type=hidden name=imgId value=$imgId />";
                print "<input type=hidden name=size value=$size />";
                print "<input type=hidden name=nbimg value=$nbImg />";
        
                print "<input type=hidden name=controller value=photoMatrix />";        
                    print "<select name=cat id=couleur>";
                print "<option></option>";
                    foreach ($catliste as $cats=>$item) {
                        if ($item == $cat) {
                            print "<option selected>$item</option>";
                        } else {
                            print "<option >$item</option>";
                        }
                    }
                print "</select>";
            ?>
            <input type="submit" value="Envoyer">
           </form>
		</ul>
  	</nav>
</div>