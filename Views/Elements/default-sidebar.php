<?php # Mise en place du menu par un parcours de la table associative
	$menu['Home']="index.php?controller=home&action=index";
	$menu['A propos']="index.php?controller=home&action=apropos";
	$menu['Voir photos']="index.php?controller=photo";
?>
<!-- Sidebar -->
<div class="s-layout__sidebar">
  	<a class="s-sidebar__trigger" href="#0">
     	<i class="fa fa-bars"></i>
  	</a>
    <nav class="s-sidebar__nav">
		<ul>
            <?php
            	foreach ($menu as $item => $act) {
                    switch ($item) {
                        case 'Home':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-home\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'A propos':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-info\"></i><em>$item</em></a></li>\n";
                            break;
                        case 'Voir photos':
                            print "<li><a class=\"s-sidebar__nav-link\" href=\"$act\"><i class=\"fa fa-images\"></i><em>$item</em></a></li>\n";
                            break;
                    }
                }
            ?>
		</ul>
  	</nav>
</div>