<?php
  
  # Notion d'image
  class Album {
    private $nom=""; 
    private Image $image();

    function __construct($nom,Image $image ) {
      $this->nom = $nom;
      $this->image = $image;
    }
    
    # Retourne l'URL de cette image
    function getImage() {
		return $this->image();
    }

    function getNom() {
      return $this->nom;
    }
  }  
?>