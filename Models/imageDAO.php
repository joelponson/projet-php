<?php
	require_once("Config/site-config.php");
	require_once("Models/database.php");
	require_once("image.php");

	# Le 'Data Access Object' d'un ensemble images
	class ImageDAO {
		
		
		private $db;
		# Tableau pour stocker tous les chemins des images
		private $imgEntry;
		
		# Lecture récursive d'un répertoire d'images
		# Ce ne sont pas des objets qui sont stockes mais juste
		# des chemins vers les images.
		private function readDir($dir) {
			# build the full path using location of the image base
			$fdir=PATH.$dir;
			if (is_dir($fdir)) {
				$d = opendir($fdir);
				while (($file = readdir($d)) !== false) {
					if (is_dir($fdir."/".$file)) {
						# This entry is a directory, just have to avoid . and .. or anything starts with '.'
						if (($file[0] != '.')) {
							# a recursive call
							$this->readDir($dir."/".$file);
						}
					} else {
						# a simple file, store it in the file list
						if (($file[0] != '.')) {
							$this->imgEntry[]="$dir/$file";
						}
					}
				}
			}
		}
		

		function __construct() {	
			$instance = ConnectDb::getInstance();
			$this->db = $instance->getConnection();
			$this->readDir("");
		}
		
		# Retourne le nombre d'images référencées dans le DAO
		function size() {
            $req = 'SELECT count(*) as nb FROM image';
			$s = $this->db->query($req);         
		    $rows = $s->fetch();          
			return $rows['nb'];
		}
		
		# Retourne un objet image correspondant à l'identifiant
		function getImage($imgId) {
            $req='SELECT * FROM image WHERE id='.$imgId;
			$s = $this->db->query($req);
			if ($s) { 
				$rows = $s->fetchAll(PDO::FETCH_ASSOC);
				return new Image(URL_PATH.PATH.$rows[0]['path'],$rows[0]['id'],$rows[0]['category'],$rows[0]['comment'],$rows[0]['liked'],$rows[0]['disliked']);
			} else {
				print "Error in getImage. id=".$imgId."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
			} 
        }
		
		
		# Retourne une image au hazard
		function getRandomImage() {
			$taille = $this->size();
			$id=rand(1, $taille);
			return $this->getImage($id);
		}
		
		# Retourne l'objet de la premiere image
		function getFirstImage() {
			return $this->getImage(1);
		}
		
		# Retourne l'image suivante d'une image
		function getNextImage(image $img) {   
			$id = $img->getId();
			if ($id < $this->size()) {
                $img = $this->getImage($id+1);
			}
			return $img;
		}
		
		# Retourne l'image précédente d'une image
		function getPrevImage(image $img) {
			$id = $img->getId();
			if ($id < $this->size() && $id >1 ) {
				$img = $this->getImage($id-1);
			}
		 	return $img;
		}
 
		# Retourne la liste des images consécutives à partir d'une image
		function getImageList(image $img,$nb) {
			# Verifie que le nombre d'image est non nul
			if (!$nb > 0) {
				debug_print_backtrace();
				trigger_error("Erreur dans ImageDAO.getImageList: nombre d'images nul");
			}
   			$id = $img->getId();
			$max = $id+$nb;
			while ($id < $this->size() && $id < $max) {
				$res[] = $this->getImage($id);
				$id++;
			}
			return $res;
		}
        
        # Retourne la liste des images consécutives à partir d'une catégorie
		function getImageCat(image $img,$nb, $Cat) {
			# Verifie que le nombre d'image est non nul
			if ($Cat == " ") {
				debug_print_backtrace();
				trigger_error("Erreur dans ImageDAO.getImageCat: Categorie null");
			}
            $id = $img->getId();
            $req='SELECT * FROM image WHERE category="'.$Cat.'" and id >='.$id.' ORDER BY id';
           	$s = $this->db->query($req);
			if ($s) { 
                $rows = $s->fetchAll(PDO::FETCH_ASSOC);
                $iteration = 0;
                
                foreach ($rows as $row)  {
                    $iteration = $iteration +1;
                    $res[] = new Image(URL_PATH.PATH.$row['path'],$row['id'],$row['category'],$row['comment'],$row['liked'],$row['disliked']);
                    if ($iteration ==$nb)  {break;}
                }

			} else {
				print "Error in getImage. id=".$imgId."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
            } 
            if($rows== null) {
                return null;
            } else {
				return $res;
			}	
		}

        # Retourne la liste des catégories
		function getCat() {
			
            $req='SELECT DISTINCT category FROM image ';
			$s = $this->db->query($req);
			if ($s) { 
                $rows = $s->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows as $row) {
                        $res[] = $row['category'];
                }

			} else {
				print "Error in getCat. id=".$imgId."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
            } 
			return $res;
		}
        # met à jour les infos d'une image
		function modifierImage($id, $CatModi, $commentModi) {		
            $req ='UPDATE image SET comment="'.$commentModi.'", category="'.$CatModi.'" where id = '.$id.';';
            try {	
 				$r = $this->db->exec($req);
			} catch (PDOException $e) {
                die("PDO Error :".$e->getMessage());
            }
		}
                # met à jour les infos d'une image
		function ajouterImage($fichier, $CatModi, $commentModi) {		 

            $req ='Insert Into image( path,category,comment,liked,disliked) VALUES("'.$fichier.'","'.$CatModi.'","'.$commentModi.'",0,0);';
            try {	
 				$r = $this->db->exec($req);
			} catch (PDOException $e) {
                die("PDO Error :".$e->getMessage());
            }
		}
        # ajoute un like à la photo
		function addLike($img) {    
            
            $like = $img->getLike(); 
            $like = $like+1;
            $req ='UPDATE image SET liked='.$like.' where id = '.$img->getId().';';				
            try {	
 				$r = $this->db->exec($req);
			} catch (PDOException $e) {
                die("PDO Error :".$e->getMessage());
            }
		}
        # ajoute un dislike à la photo
		function addDislike($img) {  
            
            $dislike = $img->getDislike();
            $dislike =$dislike +1;
            $req ='UPDATE image SET disliked='.$dislike.' where id = '.$img->getId().';';			
            try {	
 				$r = $this->db->exec($req);
			} catch (PDOException $e) {
                die("PDO Error :".$e->getMessage());
            }
		}
        function getAlbum(){
			$res = null;
            # Retourne la liste des catégories
            $req='SELECT DISTINCT nom FROM album';
			$s = $this->db->query($req);
			if ($s) { 
				$rows = $s->fetchAll(PDO::FETCH_ASSOC);
				if(isset($rows) && !empty($rows)) {
					foreach ($rows as $row) {

						$nom = $row['nom'];
						$res[$nom] = $this->getAlbumbyNom($nom);
						
					}
				}                
			} else {
				print "Error in getAlbum. id=".$imgId."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
            } 
			return $res;

        }
            
            
        function getAlbumbyNom($nomalbum){
            if ($nomalbum == " ") {
				debug_print_backtrace();
				trigger_error("Erreur dans ImageDAO.getAlbum: nom album null");
			}
           $nomalbum= strtolower($nomalbum); 
		# Verifie que le nombre d'image est non nul

            $req='SELECT * FROM image, album WHERE nom="'.$nomalbum.'" and imageid = id';
            
           	$s = $this->db->query($req);
			if ($s) { 
                $rows = $s->fetchAll(PDO::FETCH_ASSOC);
                $iteration = 0;
                foreach ($rows as $row)  {
                    $iteration = $iteration +1;
                    $res[] = new Image(URL_PATH.PATH.$row['path'],$row['id'],$row['category'],$row['comment'],$row['liked'],$row['disliked']);
                    
                }

			} else {
				print "Error in getImage. id=".$imgId."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
            }
            
            if($rows== null){
                return null;
            } else{
                
            return $res;}	
		}
        
        
        function ajouterPhoto($nomalbum, $idimage){
           $nomalbum= strtolower($nomalbum); 
            if ($nomalbum != " "|| $nomalbum != " "){
				$req ='Insert Into album( nom,imageid) VALUES("'.$nomalbum.'",'.$idimage.');';
				try {	
					$r = $this->db->exec($req);
				} catch (PDOException $e) {
					die("PDO Error :".$e->getMessage());
				}
         	}
        }
      function  enleverPhoto ($nomalbum, $idimage){
			$nomalbum= strtolower($nomalbum); 
				if ($nomalbum != " "|| $nomalbum != " "){
				$req ='delete from album  WHERE nom="'.$nomalbum.'" and imageid = '.$idimage;
				try {	
					$r = $this->db->exec($req);
				} catch (PDOException $e) {
					die("PDO Error :".$e->getMessage());
				}
         	}
        }       
       
    }	
?>