<?php 
    require_once("Config/db-config.php");
    // Singleton to connect db.
    class ConnectDb {
        // Hold the class instance.
        private static $instance = null;
        private $db;
        
        // The db connection is established in the private constructor.
        private function __construct() {
            try {
                $this->db = new PDO('mysql:host='.SERVER.';dbname='.DATABASE, USERNAME, PASSWORD);			
			} catch (PDOException $e) {
				print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
			}

        }
        
        public static function getInstance() {
            if(!self::$instance) {
                self::$instance = new ConnectDb();
            } 
            return self::$instance;
        }
        
        // Return the connect
        public function getConnection() {
            return $this->db;
        }

        // Return the query
        public function query($sql) {
            return $this->db->query($sql);
        }

        /* CES FONCTIONS SONT DESTINEES A REFACTORER LE CODE
        public function add($sql) {
            try {
                $this->db->prepare($sql);
                $this->db->exec($sql);
            } catch (PDOException $e) {
                die("PDO Error :".$e->getMessage());
            }
        }

        public function countSql($tableName, $colonne = "*") {
            return 'SELECT count('.$colonne.') as nb FROM '.$tableName.'';
        }

        public function selectSql($distinct = "", $colonne = "*", $tableName) {
            return 'SELECT '.$colonne.' '.'FROM '.$tableName;
        }

        public function selectSqlDistinct($distinct, $colonne = "*", $tableName) {
            return 'SELECT '.$distinct.' '.'FROM '.$tableName;
        }

        public function updateSql() {

        }

        public function insertSql() {

        }

        public function deleteSql() {

        }
        */
    }
?>