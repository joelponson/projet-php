<?php
  
  # Notion d'image
  class Image {
    private $url=""; 
    private $id=0;
    private $cat = "";
    private $comment = "";
    private $like = 0;
    private $dislike = 0;

    function __construct($u,$id, $cat, $comment, $li,$dis) {
      $this->url = $u;
      $this->id = $id;
      $this->cat = $cat;
      $this->comment = $comment;
      $this->like = $li;
      $this->dislike = $dis;
    }
    
    # Retourne l'URL de cette image
    function getURL() {
		  return $this->url;
    }

    function getId() {
      return $this->id;
    }

    function getCat() {
      return $this->cat;
    }

    function getComment() {
      return $this->comment;
    }

    function getLike() {
      return $this->like;
    }
    
    function getDislike() {
      return $this->dislike;
    }
  }
  
  
?>