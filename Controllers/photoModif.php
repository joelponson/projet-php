<?php
	require_once("Models/imageDAO.php");
	
	class PhotoModif {
		
		protected $imageDAO;
		
		function __construct() {
			// Ouvre le imageDAO
			$this->imageDAO = new ImageDAO();
		}
		
		// Recupere les parametres de manière globale
		// Pour toutes les actions de ce contrôleur
		protected function getParam() {
			// Recupère un éventuel no de départ
			global $imgId,$mode,$size,$imgURL,$zoom,$size,$nbImg,$controller,$catliste,$catselect,$Comment,$like,$dislike;
			if (isset($_GET["imgId"])) {
				$imgId = $_GET["imgId"];
			} else {
				$imgId = 1;
			}
            
			// Recupere le mode delete de l'interface
			if (isset($_GET["mode"])) {
				$mode = $_GET["mode"];
			} else {
				$mode = "normal";
			}
            // Regarde si une taille pour l'image est connue
            if (isset($_GET["size"])) {
                $size = $_GET["size"];
            } else {
                # sinon place une valeur de taille par défaut
                $size = 480;
            }
            // Parametre de l'action
            if (isset($_GET["zoom"])) {
                $zoom = $_GET["zoom"];
            } 
            $nbImg = 1;
            // Parametre de l'action
            if (isset($_GET["controller"])) {
                $controller = $_GET["controller"];
            }else {
                # sinon place une valeur de taille par défaut
                $controller = "photo";
            }
                        // Récupère le nombre d'images à afficher
            if (isset($_GET["cat"]) and $_GET["cat"] !="") {
                $catselect = $_GET["cat"];
            } 
            elseif (isset($_POST["cat"]) and $_POST["cat"] !=""){
                $catselect = $_POST["cat"];
            }
            else {
                # sinon débute avec 2 images
                $catselect =  "";
            } 
                        // Parametre de l'action
            if (isset($_GET["comment"])) {
                $Comment = $_GET["comment"];
            }
             elseif (isset($_POST["comment"]) and $_POST["comment"] !=""){
                $Comment = $_POST["comment"];
            }
            else {
                # sinon place une valeur de taille par défaut
                $Comment = "Vide";
            }
		}
		
		// Place au moins 1 éléments à afficher
		// à partir de $imgId
		// dans $data de manière globale
		protected function setNews() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            global $imgNext,$imgNextId, $imgPrev,$imgPrevId, $imgRand, $imgRandId;
            
			// Selection de 1 element  pour affichage
            if ($imgId ==null){$id = 1;}
			 else{$id = $imgId;}
            $catliste = $this->imageDAO->getCat();
             $img       = $this->imageDAO->getImage($id);

            $imgURL    = $img->getURL();
            $cat       = $img->getCat();
            $Comment   = $img->getComment();
            $like      = $img->getLike();
            $dislike   = $img->getDislike();
		}
		
		// LISTE DES ACTIONS DE CE CONTROLEUR
		
		// Action par défaut
		function index() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoModif.php");
		}
		
		function backward() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;;
			$this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getPrevImage($img);
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoModif.php");
		}
		
		function forward() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getNextImage($img);
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoModif.php");
		}
		
		
		function first() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img = $this->imageDAO->getFirstImage();
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");

		}
        function random() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img = $this->imageDAO->getRandomImage();
            $imgId= $img->getId();
            $this->setNews();
            $controller = "photo";
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");

		}
        function zoomm() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
            // Calcule la nouvelle taille
            $size *= $zoom;
            $controller = "photo";
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function zooml() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
            // Calcule la nouvelle taille
            $size /= $zoom;
             $controller = "photo";
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function less() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoModif.php");
		}
        function modifier() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            $this->getParam();
            
             $this->imageDAO->modifierImage($imgId, $catselect, $Comment);
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoModif.php");
		}
        
        function upload() {
            global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike;
            global $ok;
            $this->getParam();
            $dossier = 'IMG/jons/utilisateur/';
            $ok      = false;
            if(isset($_FILES['avatar']))
            { 
                $fichier = basename($_FILES['avatar']['name']);
                $taille_maxi = 100000;
                $taille = filesize($_FILES['avatar']['tmp_name']);
                $extensions = array('.png', '.gif', '.jpg', '.jpeg');
                $extension = strrchr($_FILES['avatar']['name'], '.'); 
                
                if(in_array($extension, $extensions)) //Si l'extension est dans le tableau
                {
                   
                   if($taille<$taille_maxi)
                    {
                      //On formate le nom du fichier ici...
                     $fichier = strtr($fichier, 
                          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
                          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                     $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
                     if(move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                     {
                        
                       $fichier = 'jons/utilisateur/'.$fichier;
                       $this->imageDAO->ajouterImage($fichier, $catselect, $Comment);
                         $ok = true;
                     }
                    }
                }
            }
            
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewUpload.php");
		}
		

	}
?>