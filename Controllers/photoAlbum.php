<?php
	require_once("Models/imageDAO.php");
	
	class PhotoAlbum {
		
		private $imageDAO;
		
		function __construct() {
			// Ouvre le imageDAO
			$this->imageDAO = new ImageDAO();
		}
		
		// Recupere les parametres de manière globale
		// Pour toutes les actions de ce contrôleur
		protected function getParam() {
			// Recupère un éventuel no de départ
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$album;
            $controller = "photo";
			if (isset($_GET["imgId"])) {
				$imgId = $_GET["imgId"];
			} else {
				$imgId = 1;
			}
			// Recupere le mode delete de l'interface
			if (isset($_GET["mode"])) {
				$mode = $_GET["mode"];
			} else {
				$mode = "normal";
			}
            // Regarde si une taille pour l'image est connue
            if (isset($_GET["size"])) {
                $size = $_GET["size"];
            } else {
                # sinon place une valeur de taille par défaut
                $size = 480;
            }
            // Récupère le nombre d'images à afficher
            if (isset($_GET["nbimg"]) && $_GET["nbimg"] >1) {
                $nbImg = $_GET["nbimg"];
            } else {
                # sinon débute avec 2 images
                $nbImg = 1;
            }

            // Récupère le nombre d'images à afficher
            if (isset($_GET["cat"]) and $_GET["cat"] !="") {
                $catselect = $_GET["cat"];
            } else {
                # sinon débute avec 2 images
                $catselect =  " ";
            } 
            // Récupère le nombre d'images à afficher
            if (isset($_GET["album"]) and $_GET["album"] !="") {
                $album = $_GET["album"];
            } else {
                # sinon débute avec 2 images
                $album =  " ";
            } 
            
     
		}
		
		// Place au moins 1 éléments à afficher
		// à partir de $imgId
		// dans $data de manière globale
		protected function setNews() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste,$album;
			// Selection de 1 element  pour affichage
            if ($imgId ==null){$id = 1;}
			 else{$id = $imgId;}
              $img = $this->imageDAO->getImage($id);
     
            $catliste = $this->imageDAO->getCat();
            # Transforme cette liste en liste de couples (tableau a deux valeurs)
            # contenant l'URL de l'image et l'URL de l'action sur cette image
          if ($album = "" || $album = " ") {             
            # Calcul la liste des images à afficher
            $imgLst= $this->imageDAO->getAlbum();
            
            foreach ($imgLst as $item =>$valeur) {
                
                foreach ($valeur as $i) {
                    # l'identifiant de cette image $i
                    $iId=$i->getId();
                    # Ajoute à imgMatrixURL 
                    #  0 : l'URL de l'image
                    #  1 : l'URL de l'action lorsqu'on clique sur l'image : la visualiser seul
                    #  2 : lla catégorie
                    $imgMatrixURL[] = array($i->getURL(),"index.php?controller=photoAlbum&imgId=$iId&album=$item&action=affiche",$item);
                    $imgId = $iId;
                    break;
                }
                }
              }
            else{
                # Calcul la liste des images à afficher
                $imgLst= $this->imageDAO->getAlbumbyNom($album);
                if (isset($imgLst)) {
                    foreach ($imgLst as $i) {
                        # l'identifiant de cette image $i
                        $iId=$i->getId();
                        # Ajoute à imgMatrixURL 
                        #  0 : l'URL de l'image
                        #  1 : l'URL de l'action lorsqu'on clique sur l'image : la visualiser seul
                        #  2 : lla catégorie
                        $imgMatrixURL[] = array($i->getURL(),"index.php?controller=photoAlbum&imgId=$iId&album=$album",$album);
                        $imgId = $iId;
                    }
                }    
		    }
        }
		// LISTE DES ACTIONS DE CE CONTROLEUR
		
		// Action par défaut
		function index() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste,$album;
            $this->getParam();
            $album = "";
			$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoAlbum.php");
		}
		// 
		function ajout() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste,$album;
			$this->getParam();
            $img = $this->imageDAO->ajouterPhoto($album, $imgId);
            $album = " ";
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoAlbum.php");
		}
		// 
        function affiche() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste,$album;
			$this->getParam();
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");
		}
        function enlever () {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste,$album;
			$this->getParam();
            $this->imageDAO->enleverPhoto($album, $imgId);
            $album = " ";
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoAlbum.php");
		}
	}
?>