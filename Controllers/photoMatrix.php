<?php
	require_once("Models/imageDAO.php");
	
	class PhotoMatrix {
		
		protected $imageDAO;
		
		function __construct() {
			// Ouvre le imageDAO
			$this->imageDAO = new ImageDAO();
		}
		
		// Recupere les parametres de manière globale
		// Pour toutes les actions de ce contrôleur
		protected function getParam() {
			// Recupère un éventuel no de départ
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect;
            $controller = "photoMatrix";
			if (isset($_GET["imgId"])) {
				$imgId = $_GET["imgId"];
			} else {
				$imgId = 1;
			}
			// Recupere le mode delete de l'interface
			if (isset($_GET["mode"])) {
				$mode = $_GET["mode"];
			} else {
				$mode = "normal";
			}
            // Regarde si une taille pour l'image est connue
            if (isset($_GET["size"])) {
                $size = $_GET["size"];
            } else {
                # sinon place une valeur de taille par défaut
                $size = 480;
            }
            // Récupère le nombre d'images à afficher
            if (isset($_GET["nbimg"]) && $_GET["nbimg"] >= 1) {
                $nbImg = $_GET["nbimg"];
            } else {
                # sinon débute avec 2 images
                $nbImg = 2;
            }

            // Récupère le nombre d'images à afficher
            if (isset($_GET["cat"]) and $_GET["cat"] !="") {
                $catselect = $_GET["cat"];
            } else {
                # sinon débute avec 2 images
                $catselect =  " ";
            } 
     
		}
		
		// Place au moins 1 éléments à afficher
		// à partir de $imgId
		// dans $data de manière globale
		protected function setNews() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
			// Selection de 1 element  pour affichage
            if ($imgId ==null){$id = 1;}
			 else{$id = $imgId;}
                $img = $this->imageDAO->getImage($id);
            if ($catselect == " " ||$catselect == ""  ){            
                # Calcul la liste des images à afficher
                $imgLst= $this->imageDAO->getImageList($img,$nbImg);
                
                 if ($imgLst == null){
                $img = $this->imageDAO->getFirstImage();
                  $imgLst= $this->imageDAO->getImageList($img,$nbImg);
            }
            }else
            {
                $imgLst= $this->imageDAO->getImageCat($img,$nbImg,$catselect);
                
                if ($imgLst == null){
                    $img = $this->imageDAO->getFirstImage();
                    $imgLst= $this->imageDAO->getImageCat($img,$nbImg,$catselect);
                }
            }

            $catliste = $this->imageDAO->getCat();
            # Transforme cette liste en liste de couples (tableau a deux valeurs)
            # contenant l'URL de l'image et l'URL de l'action sur cette image
            foreach ($imgLst as $i) {
                # l'identifiant de cette image $i
                $iId=$i->getId();
                # Ajoute à imgMatrixURL 
                #  0 : l'URL de l'image
                #  1 : l'URL de l'action lorsqu'on clique sur l'image : la visualiser seul
                $imgMatrixURL[] = array($i->getURL(),"index.php?controller=photoModif&imgId=$iId");
                $imgId = $iId;
            }
            
		}
		
		// LISTE DES ACTIONS DE CE CONTROLEUR
		
		// Action par défaut
		function index() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
			$this->getParam();
			$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");
		}
		// 
		function backward() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
			$this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getPrevImage($img);
            $imgId= $img->getId();
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");
		}
		// 
		function forward() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
            $this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getNextImage($img);
            $imgId= $img->getId();
     		$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");
		}
		
		
		function first() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
            $this->getParam();
            $img = $this->imageDAO->getFirstImage();
            $imgId= $img->getId();
       		$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");

		}
        function random() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
            $this->getParam();
            $img = $this->imageDAO->getRandomImage();
            $imgId= $img->getId();
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");

		}
        function more() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
            $this->getParam();
            $nbImg = $nbImg*2;
		    $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhotoMatrix.php");

		} 
        function less() {
			global $imgId,$mode,$size,$imgURL,$imgMatrixURL,$nbImg,$controller,$catselect,$catliste;
            $this->getParam();
            if ($nbImg > 1){
                $nbImg = $nbImg/2;
                $this->setNews();
                require_once("Views/viewPhotoMatrix.php");
            } else{
                $controller="photo";
                $this->setNews();
                // Selectionne et charge la vue
                require_once("Views/viewPhoto.php");  
            }
		}  

	}
?>