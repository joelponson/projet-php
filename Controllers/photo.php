<?php
	require_once("Models/imageDAO.php");
	
	class Photo {
		
		protected $imageDAO;
		
		function __construct() {
			// Ouvre le imageDAO
			$this->imageDAO = new ImageDAO();
		}
		
		// Recupere les parametres de manière globale
		// Pour toutes les actions de ce contrôleur
		protected function getParam() {
			// Recupère un éventuel no de départ
			global $imgId,$mode,$size,$imgURL,$zoom,$size,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
			if (isset($_GET["imgId"])) {
				$imgId = $_GET["imgId"];
			} else {
				$imgId = 1;
			}
			// Recupere le mode delete de l'interface
			if (isset($_GET["mode"])) {
				$mode = $_GET["mode"];
			} else {
				$mode = "normal";
			}
            // Regarde si une taille pour l'image est connue
            if (isset($_GET["size"])) {
                $size = $_GET["size"];
            } else {
                # sinon place une valeur de taille par défaut
                $size = 480;
            }
            // Parametre de l'action
            if (isset($_GET["zoom"])) {
                $zoom = $_GET["zoom"];
            } 
            $nbImg = 1;
            // Parametre de l'action
            if (isset($_GET["controller"])) {
                $controller = "photo";
            }else {
                # sinon place une valeur de taille par défaut
                $controller = "photo";
            }
            // Récupère le nombre d'images à afficher
            if (isset($_GET["album"]) and $_GET["album"] !="") {
                $album = $_GET["album"];
            } else {
                # sinon débute avec 2 images
                $album =  " ";
            } 
            

		}
		
		// Place au moins 1 éléments à afficher
		// à partir de $imgId
		// dans $data de manière globale
		protected function setNews() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
//            global $imgNext,$imgNextId, $imgPrev,$imgPrevId, $imgRand, $imgRandId;
			// Selection de 1 element  pour affichage
            if ($imgId ==null){$id = 1;}
			 else{$id = $imgId;}
            $catliste = $this->imageDAO->getCat();
            $img       = $this->imageDAO->getImage($id);
            $albumListe= $this->imageDAO->getAlbum();
            $imgURL    = $img->getURL();
            $cat       = $img->getCat();
            $Comment   = $img->getComment();
            $like      = $img->getLike();
            $dislike   = $img->getDislike();

		}
		
		// LISTE DES ACTIONS DE CE CONTROLEUR
		
		// Action par défaut
		function index() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
           // $img = $this->imageDAO->getFirstImage();
            $img = $this->imageDAO->getImage($imgId);
            $imgId= $img->getId();
            $this->setNews();
            //echo $cat;
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
		
		function backward() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
			$this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getPrevImage($img);
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
		
		function forward() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img = $this->imageDAO->getImage($imgId);
			$img = $this->imageDAO->getNextImage($img);
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
		
		
		function first() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img = $this->imageDAO->getFirstImage();
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");

		}
        function random() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img = $this->imageDAO->getRandomImage();
            $imgId= $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");

		}
        function zoomm() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
            // Calcule la nouvelle taille
            $size *= $zoom;
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function zooml() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
            // Calcule la nouvelle taille
            $size /= $zoom;
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function less() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId);
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function like() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId); 
            $this->imageDAO->addLike($img);
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function dislike() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $img    = $this->imageDAO->getImage($imgId); 
            $this->imageDAO->addDislike($img);
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}
        function ajout() {
			global $imgId,$mode,$data,$imgURL, $size,$cat,$Comment,$zoom,$nbImg,$controller,$catliste,$catselect,$like,$dislike,$albumListe,$album;
            $this->getParam();
            $this->imageDAO->ajouterPhoto($album, $imgId);
            $img    = $this->imageDAO->getImage($imgId); 
            $imgId  = $img->getId();
            $this->setNews();
			// Selectionne et charge la vue
			require_once("Views/viewPhoto.php");
		}


        
		

	}
?>