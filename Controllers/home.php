<?php
	require_once("Models/imageDAo.php");
	
	class Home {	
		// Recupere les parametres de manière globale
		// Pour toutes les actions de ce contrôleur
		protected function getParam() {
			// Recupère un éventuel no de départ
			global $from,$mode;
			if (isset($_GET["from"])) {
				$from = $_GET["from"];
			} else {
				$from = 1;
			}
			// Recupere le mode delete de l'interface
			if (isset($_GET["mode"])) {
				$mode = $_GET["mode"];
			} else {
				$mode = "normal";
			}
		}

		// LISTE DES ACTIONS DE CE CONTROLEUR	
		// Action par défaut
		function index() {
			global $from,$mode,$data;
			$this->getParam();
			//$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/index.php");
		}

        // Action par défaut
		function apropos() {
			global $from,$mode,$data;
			$this->getParam();
			//$this->setNews();
			// Selectionne et charge la vue
			require_once("Views/aPropos.php");
		}
	}
?>